import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {applyStyle} from '../store/actions';
import './ControlPanel.css';

class ControlPanel extends Component {
    render() {
        const { modify } = this.props;

        const btnActions = [
            {modifier: 'b', button: <b>B</b>},
            {modifier: 'i', button: <i>I</i>},
            {modifier: 'u', button: <u>U</u>},
        ];

        return (
            <div id="control-panel">
                <div id="format-actions">
                    {btnActions.map(action => 
                        <button className="format-action" type="button" onClick={() => modify(action.modifier)}>{action.button}</button>
                    )}
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    modify: (modifier) => {
        console.log(modifier);
        dispatch(applyStyle(modifier));
    },
});

export default connect(null, mapDispatchToProps)(ControlPanel);
