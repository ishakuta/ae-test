export const applyStyle = modifier => ({
  type: 'APPLY_STYLE',
  modifier
});

export const selectText = selection => ({
    type: 'TEXT_SELECTED',
    selection
});
