const textEditor = (state = {operations: []}, action) => {
    switch (action.type) {
      case 'APPLY_STYLE':
        const newState = { ...state };
        const lastOp = newState.operations[newState.operations.length-1];
        lastOp.modifier = action.modifier;

        return newState;

      case 'TEXT_SELECTED':
        const { startOffset, endOffset } = action.selection;

        return {
            ...state,
            operations: [
                ...state.operations,
                {startOffset, endOffset}
            ],
        };
      default:
            return state;
    }
}

export default textEditor;
