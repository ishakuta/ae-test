import React, {Component} from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import './App.css';
import ControlPanel from "./control-panel/ControlPanel";
import FileZone from "./file-zone/FileZone";
import getMockText from './text.service';
import rootReducer from './store/reducer';

const store = createStore(rootReducer);

class App extends Component {
    state = {
        text: '',
    };

    componentDidMount() {
        getMockText().then(result => {
            this.setState({text: result});
        });
    }

    render() {
        return (
            <Provider store={store}>
                <div className="App">
                    <header>
                        <span>Simple Text Editor</span>
                    </header>
                    <main>
                        <ControlPanel/>
                        <FileZone text={this.state.text} />
                    </main>
                </div>
            </Provider>
        );
    }
}

export default App;
