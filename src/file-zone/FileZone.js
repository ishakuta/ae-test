import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { selectText } from '../store/actions';
import './FileZone.css';

class FileZone extends Component {
    static propTypes = {
        text: PropTypes.string,
        selectText: PropTypes.func.isRequired,
        operations: PropTypes.arrayOf(PropTypes.object).isRequired
    };

    static defaultProps = {
        text: '',
    };

    constructor(props) {
        super(props);
        this.state = {
          text: props.text,
        };
      }

    onMouseUp = () => {
        const selObj = window.getSelection(); 
        const selRange = selObj.getRangeAt(0);

        const selectedText = selObj.toString().trim();
        if (selectedText.length > 0) {
            this.props.selectText(selRange);
    
            console.log(selectedText, selRange.startOffset, selRange.endOffset);
        }
    }

    render() {
        const { text, operations } = this.props;
        const formattedText = operations.reduce((accum, operation) => {
            const {startOffset, endOffset} = operation;
        }, text);

        return (
            <div id="file-zone">
                <div id="file" onMouseUp={this.onMouseUp}>
                    {formattedText}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    operations: state.operations,
});

const mapDispatchToProps = dispatch => ({
    selectText: selection => {
        dispatch(selectText(selection));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(FileZone);
